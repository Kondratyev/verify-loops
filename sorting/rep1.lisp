(in-package "ACL2")

(include-book "std/util/defrule" :dir :system)
(include-book "centaur/fty/top" :dir :system)
(include-book "std/util/bstar" :dir :system)
(include-book "std/lists/top" :dir :system)
(include-book "std/basic/inductions" :dir :system)
(include-book "tools/with-arith5-help" :dir :system)
(local (allow-arith5-help))

(include-book "permut")
(include-book "ordered")
(include-book "range")
(include-book "rep2")

(fty::defprod frame1
    ((loop-break booleanp)
     (i integerp)
     (j integerp)
     (k integerp)
     (a integer-listp)))

(fty::defprod envir1
    ((lower-bound integerp)))

(define frame1-init
    ((i integerp)
     (j integerp)
     (k integerp)
     (a integer-listp))
    :returns (fr1 frame1-p)
    (make-frame1
        :loop-break nil
        :i i
        :j j
        :k k
        :a a
    )
    ///
    (fty::deffixequiv frame1-init))

(define envir1-init
    ((lower-bound integerp))
    :returns (env1 envir1-p)
    (make-envir1
        :lower-bound lower-bound
    )
    ///
    (fty::deffixequiv envir1-init))

(define rep1
    ((iteration natp)
     (env1 envir1-p)
     (fr1 frame1-p))
    :measure (nfix iteration)
    :verify-guards nil
    :returns (upd-fr1 frame1-p)
    (b*
        ((iteration (nfix iteration))
         (env1 (envir1-fix env1))
         (fr1 (frame1-fix fr1))        
         ((when (zp iteration)) fr1)
         (fr1 (rep1 (- iteration 1) env1 fr1))
         ((when (frame1->loop-break fr1)) fr1)
         (fr1
             (change-frame1
                 fr1
                 :k
                 (nth
                      (-
                          (+
                              iteration
                              (envir1->lower-bound env1)
                          )
                          1
                      )
                      (frame1->a fr1)
                 )
             )
         )
         (fr1
             (change-frame1
                 fr1
                 :j
                 (-
                     (+
                         iteration
                         (envir1->lower-bound env1)
                     )
                     1
                 )
             )
         )
         (fr2
             (rep2
                 (+ (frame1->j fr1) 1)
                 (envir2-init
                      (+ (frame1->j fr1) 1)
                      (frame1->k fr1)
                 )
                 (frame2-init
                      (frame1->j fr1)
                      (frame1->a fr1)
                 )
             )
         )
         (fr1
              (change-frame1
                 fr1
                 :j
                 (frame2->j fr2)
              )
         )
         (fr1
              (change-frame1
                 fr1
                 :a
                 (frame2->a fr2)
              )
         )
         (fr1
             (change-frame1
                 fr1
                 :a
                 (update-nth
                     (+ (frame1->j fr1) 1)
                     (frame1->k fr1)
                     (frame1->a fr1)
                 )
             )
         )
         (fr1
             (change-frame1 fr1
                 :i
                 (+
                     iteration
                     (envir1->lower-bound env1)
                 )
             )
         )
        )
    fr1)
    ///
    (defrule rep1-lemma-1
        (equal
            (envir1-fix (envir1-fix env1))
            (envir1-fix env1)
        )
    )
    (defrule rep1-lemma-2
        (equal
            (frame1-fix (frame1-fix fr1))
            (frame1-fix fr1)
        )
    )
    (defrule rep1-lemma-3
        (equal
            (rep1 iteration (envir1-fix env1) fr1)
            (rep1 iteration env1 fr1)
        )
        :hints (("Goal"
                 :expand
                 ((rep1 iteration (envir1-fix env1) fr1)
                 (rep2 iteration env1 fr1))))
        :use rep1-lemma-1
        :do-not-induct t
    )
    (defrule rep1-lemma-4
        (equal
            (rep1 iteration env1 (frame1-fix fr1))
            (rep1 iteration env1 fr1)
        )
        :hints (("Goal"
                 :expand
                 ((rep1 iteration env1 (frame1-fix fr1))
                 (rep1 iteration env1 fr1))))
        :use rep1-lemma-2
        :do-not-induct t
    )
    (defrule rep1-lemma-5
        (integer-listp (frame1->a (rep1 iteration env1 fr1)))
        :induct (dec-induct iteration) 
    )
    (defrule rep1-lemma-6
        (implies
            (integerp iteration)
            (equal
                                  (+
                                      (-
                                          (-
                                              (+
                                                  iteration
                                                  (envir1->lower-bound env1)
                                              )
                                              1
                                          )
                                          1
                                      )
                                      1
                                  )
                (envir2->upper-bound
                              (envir2-init
                                  (+
                                      (-
                                          (-
                                              (+
                                                  iteration
                                                  (envir1->lower-bound env1)
                                              )
                                              1
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (nth
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      (frame1->a (rep1 (- iteration 1) env1 fr1))
                                  )
                              )
                )
            )
        )
        :disable
        (
            rep2-lemma-6
            rep2-lemma-7
            sublist-12
        )
        :enable
        (
            envir2
            envir2-fix
            envir2-init
            envir2->upper-bound
            frame2
            frame2-fix
            frame2-init
            frame2->j
            ifix
            integer-list-fix
        )
        :do-not-induct t
    )
    (defrule rep1-lemma-7
        (implies
            (integerp iteration)
            (equal
                                  (-
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      1
                                  )
                (frame2->j
                              (frame2-init
                                  (-
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (frame1->a (rep1 (- iteration 1) env1 fr1))
                              )
                )
            )
        )
        :disable
        (
            rep2-lemma-6
            rep2-lemma-7
            sublist-12
        )
        :enable
        (
            envir2
            envir2-fix
            envir2-init
            envir2->upper-bound
            frame2
            frame2-fix
            frame2-init
            frame2->j
            ifix
            integer-list-fix
        )
        :do-not-induct t
    )
    (defrule rep1-lemma-8
            (equal
                (frame1->a (rep1 (- iteration 1) env1 fr1))
                (frame2->a
                              (frame2-init
                                  (-
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (frame1->a (rep1 (- iteration 1) env1 fr1))
                              )
                )               
            )
        :disable
        (
            rep2-lemma-6
            rep2-lemma-7
            sublist-12
        )
        :enable
        (
            envir2
            envir2-fix
            envir2-init
            envir2->upper-bound
            frame2
            frame2-fix
            frame2-init
            frame2->j
            frame2->a
            ifix
            integer-list-fix
        )
        :do-not-induct t
    ))
)
