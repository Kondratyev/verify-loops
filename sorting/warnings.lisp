(in-package "ACL2")

(include-book "std/util/defrule" :dir :system)
(include-book "centaur/fty/top" :dir :system)
(include-book "std/util/bstar" :dir :system)
(include-book "std/lists/top" :dir :system)
(include-book "std/basic/inductions" :dir :system)
(include-book "tools/with-arith5-help" :dir :system)
(local (allow-arith5-help))

(include-book "sublist")
(include-book "range")
(include-book "permut")
(include-book "ordered")
(include-book "rep2")
(include-book "rep1")
(include-book "vc-1")

(defrule warnings-lemma-1
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
            (natp i)
            (<= 1 i)
            (< i n)
            (natp iteration)
            (<= 1 iteration)
            (<= iteration (+ i 1))
        )
        (frame2->loop-break
            (rep2
                iteration
                (envir2-init
                    (+
                        i
                        1
                    )
                    (nth
                        i
                        (frame1->a
                            (rep1
                                (- i 1)
                                (envir1-init
                                    1
                                )
                                (frame1-init
                                    1
                                    0
                                    0
                                    a
                                )
                            )
                        )                           
                    )
                )
                (frame2-init
                    i
                    (frame1->a
                        (rep1
                            (- i 1)
                            (envir1-init
                                1
                            )
                            (frame1-init
                                1
                                0
                                0
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :induct (dec-induct iteration)
    :hints
    (
        ("Subgoal *1/2"
            :use
            (
                (:instance
                    sublist-12
                    (
                        i
                        i
                    )
                    (
                        u
                        (frame1->a
                            (rep1
                                (- i 1)
                                (envir1-init
                                    1
                                )
                                (frame1-init
                                    1
                                    0
                                    0
                                    a
                                )
                            )
                        )
                    )
                )
            )
        )
    )
    :enable
    (
        rep2
        frame2-init
        frame2->j
        frame2->a-of-frame2
        envir2-init
        envir2->upper-bound
        envir2->k
    )
    :disable
    (
        rep1-lemma-8
        rep2-lemma-12
        vc-1-lemma-4
        vc-1-lemma-16
        vc-1-lemma-62
        vc-1-lemma-63
        vc-1-lemma-68
    )
)

(defrule warnings-lemma-2
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
            (natp i)
            (<= 1 i)
            (< i n)
        )
        (frame2->loop-break
            (rep2
                1
                (envir2-init
                    (+
                        i
                        1
                    )
                    (nth
                        i
                        (frame1->a
                            (rep1
                                (- i 1)
                                (envir1-init
                                    1
                                )
                                (frame1-init
                                    1
                                    0
                                    0
                                    a
                                )
                            )
                        )                           
                    )
                )
                (frame2-init
                    i
                    (frame1->a
                        (rep1
                            (- i 1)
                            (envir1-init
                                1
                            )
                            (frame1-init
                                1
                                0
                                0
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :use
    (
        (:instance
            warnings-lemma-1
            (
                iteration
                1
            )
        )
    )
)

(defrule warnings-lemma-3
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
            (natp i)
            (<= 1 i)
            (< i n)
        )
        (frame2->loop-break
            (rep2
                (+
                    i
                    1
                )
                (envir2-init
                    (+
                        i
                        1
                    )
                    (nth
                        i
                        (frame1->a
                            (rep1
                                (- i 1)
                                (envir1-init
                                    1
                                )
                                (frame1-init
                                    1
                                    0
                                    0
                                    a
                                )
                            )
                        )                           
                    )
                )
                (frame2-init
                    i
                    (frame1->a
                        (rep1
                            (- i 1)
                            (envir1-init
                                1
                            )
                            (frame1-init
                                1
                                0
                                0
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :use
    (
        (:instance
            warnings-lemma-1
            (
                iteration
                (+
                    i
                    1
                )
            )
        )
    )
    :disable
    (
        rep1-lemma-8
        rep2-lemma-12
        vc-1-lemma-4
        vc-1-lemma-16
        vc-1-lemma-62
        vc-1-lemma-63
        vc-1-lemma-68
    )
)

(defrule warnings-lemma-4
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
            (natp i)
            (<= 1 i)
            (< i n)
            (natp iteration)
            (<= 1 iteration)
            (<= iteration (+ i 1))
        )
        (equal
            (frame2->j
                (rep2
                    iteration
                    (envir2-init
                        (+
                            i
                            1
                        )
                        (nth
                            i
                            (frame1->a
                                (rep1
                                    (- i 1)
                                    (envir1-init
                                        1
                                    )
                                    (frame1-init
                                        1
                                        0
                                        0
                                        a
                                    )
                                )
                            )                           
                        )
                    )
                    (frame2-init
                        i
                        (frame1->a
                            (rep1
                                (- i 1)
                                (envir1-init
                                    1
                                )
                                (frame1-init
                                    1
                                    0
                                    0
                                    a
                                )
                            )
                        )
                    )
                )
            )
            i
        )
    )
    :induct (dec-induct iteration)
    :hints
    (
        ("Subgoal *1/2"
            :use
            (
                (:instance
                    sublist-12
                    (
                        i
                        i
                    )
                    (
                        u
                        (frame1->a
                            (rep1
                                (- i 1)
                                (envir1-init
                                    1
                                )
                                (frame1-init
                                    1
                                    0
                                    0
                                    a
                                )
                            )
                        )
                    )
                )
                (:instance
                    warnings-lemma-1
                    (iteration
                        (-
                            iteration
                            1
                        )
                    )
                )
            )
        )
    )
    :enable
    (
        rep2
        frame2-init
        frame2->j-of-frame2
        frame2->a-of-frame2
        envir2-init
        envir2->upper-bound
        envir2->k
    )
    :disable
    (
        rep1-lemma-8
        rep2-lemma-12
        vc-1-lemma-4
        vc-1-lemma-16
        vc-1-lemma-62
        vc-1-lemma-63
        vc-1-lemma-68
    )
)

(defrule warnings-lemma-5
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
            (natp i)
            (<= 1 i)
            (< i n)
        )
        (equal
            (frame2->j
                (rep2
                    1
                    (envir2-init
                        (+
                            i
                            1
                        )
                        (nth
                            i
                            (frame1->a
                                (rep1
                                    (- i 1)
                                    (envir1-init
                                        1
                                    )
                                    (frame1-init
                                        1
                                        0
                                        0
                                        a
                                    )
                                )
                            )                           
                        )
                    )
                    (frame2-init
                        i
                        (frame1->a
                            (rep1
                                (- i 1)
                                (envir1-init
                                    1
                                )
                                (frame1-init
                                    1
                                    0
                                    0
                                    a
                                )
                            )
                        )
                    )
                )
            )
            i
        )
    )
    :use
    (
        (:instance
            warnings-lemma-4
            (iteration
                1
            )
        )
    )
)

(defrule warnings-lemma-6
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
            (natp i)
            (<= 1 i)
            (< i n)
        )
        (equal
            (frame2->j
                (rep2
                    (+
                        i
                        1
                    )
                    (envir2-init
                        (+
                            i
                            1
                        )
                        (nth
                            i
                            (frame1->a
                                (rep1
                                    (- i 1)
                                    (envir1-init
                                        1
                                    )
                                    (frame1-init
                                        1
                                        0
                                        0
                                        a
                                    )
                                )
                            )                           
                        )
                    )
                    (frame2-init
                        i
                        (frame1->a
                            (rep1
                                (- i 1)
                                (envir1-init
                                    1
                                )
                                (frame1-init
                                    1
                                    0
                                    0
                                    a
                                )
                            )
                        )
                    )
                )
            )
            i
        )
    )
    :use
    (
        (:instance
            warnings-lemma-4
            (iteration
                (+
                    i
                    1
                )
            )
        )
    )
    :disable
    (
        rep1-lemma-8
        rep2-lemma-12
        vc-1-lemma-4
        vc-1-lemma-16
        vc-1-lemma-62
        vc-1-lemma-63
        vc-1-lemma-68
    )
)

(defrule warnings-lemma-7
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
            (natp i)
            (<= 1 i)
            (< i n)
        )
        (and
            (equal
                (frame2->j
                    (rep2
                        (+
                            i
                            1
                        )
                        (envir2-init
                            (+
                                i
                                1
                            )
                            (nth
                                i
                                (frame1->a
                                    (rep1
                                        (- i 1)
                                        (envir1-init
                                            1
                                        )
                                        (frame1-init
                                            1
                                            0
                                            0
                                           a
                                        )
                                    )
                                )                           
                            )
                        )
                        (frame2-init
                            i
                            (frame1->a
                                (rep1
                                    (- i 1)
                                    (envir1-init
                                        1
                                    )
                                    (frame1-init
                                        1
                                        0
                                        0
                                        a
                                    )
                                )
                            )
                        )
                    )
                )
                i
            )
            (frame2->loop-break
                (rep2
                    (+
                        i
                        1
                    )
                    (envir2-init
                        (+
                            i
                            1
                        )
                        (nth
                            i
                            (frame1->a
                                (rep1
                                    (- i 1)
                                    (envir1-init
                                        1
                                    )
                                    (frame1-init
                                        1
                                        0
                                        0
                                        a
                                    )
                                )
                            )                           
                        )
                    )
                    (frame2-init
                        i
                        (frame1->a
                            (rep1
                                (- i 1)
                                (envir1-init
                                    1
                                )
                                (frame1-init
                                    1
                                    0
                                    0
                                    a
                                )
                            )
                        )
                    )
                )
            )
        )
    )
    :use
    (
        warnings-lemma-3
        warnings-lemma-6
    )
    :disable
    (
        rep1-lemma-8
        rep2-lemma-12
        vc-1-lemma-4
        vc-1-lemma-16
        vc-1-lemma-62
        vc-1-lemma-63
        vc-1-lemma-68
    )
)

(defrule warnings-lemma-8
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
            (natp i)
            (<= 1 i)
            (< i n)
        )
        (and
            (equal
                (frame2->j
                    (rep2
                        1
                        (envir2-init
                            (+
                                i
                                1
                            )
                            (nth
                                i
                                (frame1->a
                                    (rep1
                                        (- i 1)
                                        (envir1-init
                                            1
                                        )
                                        (frame1-init
                                            1
                                            0
                                            0
                                           a
                                        )
                                    )
                                )                           
                            )
                        )
                        (frame2-init
                            i
                            (frame1->a
                                (rep1
                                    (- i 1)
                                    (envir1-init
                                        1
                                    )
                                    (frame1-init
                                        1
                                        0
                                        0
                                        a
                                    )
                                )
                            )
                        )
                    )
                )
                i
            )
            (frame2->loop-break
                (rep2
                    1
                    (envir2-init
                        (+
                            i
                            1
                        )
                        (nth
                            i
                            (frame1->a
                                (rep1
                                    (- i 1)
                                    (envir1-init
                                        1
                                    )
                                    (frame1-init
                                        1
                                        0
                                        0
                                        a
                                    )
                                )
                            )                           
                        )
                    )
                    (frame2-init
                        i
                        (frame1->a
                            (rep1
                                (- i 1)
                                (envir1-init
                                    1
                                )
                                (frame1-init
                                    1
                                    0
                                    0
                                    a
                                )
                            )
                        )
                    )
                )
            )
        )
    )
    :use
    (
        warnings-lemma-2
        warnings-lemma-5
    )
    :disable
    (
        rep1-lemma-8
        rep2-lemma-12
        vc-1-lemma-4
        vc-1-lemma-16
        vc-1-lemma-62
        vc-1-lemma-63
        vc-1-lemma-68
    )
)

(defrule warnings-lemma-9
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
            (natp i)
            (<= 1 i)
            (< i n)
            (natp iteration)
            (<= 1 iteration)
            (<= iteration (+ i 1))
        )
        (and
            (equal
                (frame2->j
                    (rep2
                        iteration
                        (envir2-init
                            (+
                                i
                                1
                            )
                            (nth
                                i
                                (frame1->a
                                    (rep1
                                        (- i 1)
                                        (envir1-init
                                            1
                                        )
                                        (frame1-init
                                            1
                                            0
                                            0
                                           a
                                        )
                                    )
                                )                           
                            )
                        )
                        (frame2-init
                            i
                            (frame1->a
                                (rep1
                                    (- i 1)
                                    (envir1-init
                                        1
                                    )
                                    (frame1-init
                                        1
                                        0
                                        0
                                        a
                                    )
                                )
                            )
                        )
                    )
                )
                i
            )
            (frame2->loop-break
                (rep2
                    iteration
                    (envir2-init
                        (+
                            i
                            1
                        )
                        (nth
                            i
                            (frame1->a
                                (rep1
                                    (- i 1)
                                    (envir1-init
                                        1
                                    )
                                    (frame1-init
                                        1
                                        0
                                        0
                                        a
                                    )
                                )
                            )                           
                        )
                    )
                    (frame2-init
                        i
                        (frame1->a
                            (rep1
                                (- i 1)
                                (envir1-init
                                    1
                                )
                                (frame1-init
                                    1
                                    0
                                    0
                                    a
                                )
                            )
                        )
                    )
                )
            )
        )
    )
    :use
    (
        warnings-lemma-1
        warnings-lemma-4
    )
    :disable
    (
        rep1-lemma-8
        rep2-lemma-12
        vc-1-lemma-4
        vc-1-lemma-16
        vc-1-lemma-62
        vc-1-lemma-63
        vc-1-lemma-68
    )
)

(defrule warnings-lemma-10
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
            (natp i)
            (<= 1 i)
            (< i n)
            (natp iteration)
            (<= 1 iteration)
            (<= iteration (+ i 1))
        )
        (equal
            (frame1->a
                (rep1
                    (- i 1)
                    (envir1-init
                        1
                    )
                    (frame1-init
                        1
                        0
                        0
                        a
                    )
                )
            )
            (frame2->a
                (rep2
                    iteration
                    (envir2-init
                        (+
                            i
                            1
                        )
                        (nth
                            i
                            (frame1->a
                                (rep1
                                    (- i 1)
                                    (envir1-init
                                        1
                                    )
                                    (frame1-init
                                        1
                                        0
                                        0
                                        a
                                    )
                                )
                            )                           
                        )
                    )
                    (frame2-init
                        i
                        (frame1->a
                            (rep1
                                (- i 1)
                                (envir1-init
                                    1
                                )
                                (frame1-init
                                    1
                                    0
                                    0
                                    a
                                )
                            )
                        )
                    )
                )
            )
        )
    )
    :induct (dec-induct iteration)
    :hints
    (
        ("Subgoal *1/2"
            :use
            (
                (:instance
                    sublist-12
                    (
                        i
                        i
                    )
                    (
                        u
                        (frame1->a
                            (rep1
                                (- i 1)
                                (envir1-init
                                    1
                                )
                                (frame1-init
                                    1
                                    0
                                    0
                                    a
                                )
                            )
                        )
                    )
                )
                (:instance
                    warnings-lemma-1
                    (iteration
                        (-
                            iteration
                            1
                        )
                    )
                )
            )
        )
    )
    :enable
    (
        rep2
        frame2-init
        frame2->j-of-frame2
        frame2->a-of-frame2
        envir2-init
        envir2->upper-bound
        envir2->k
    )
    :disable
    (
        rep1-lemma-8
        rep2-lemma-12
        vc-1-lemma-4
        vc-1-lemma-16
        vc-1-lemma-62
        vc-1-lemma-63
        vc-1-lemma-68
    )
)

(defrule warnings-lemma-11
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
            (natp i)
            (<= 1 i)
            (< i n)
        )
        (equal
            (frame1->a
                (rep1
                    (- i 1)
                    (envir1-init
                        1
                    )
                    (frame1-init
                        1
                        0
                        0
                        a
                    )
                )
            )
            (frame2->a
                (rep2
                    (+
                        i
                        1
                    )
                    (envir2-init
                        (+
                            i
                            1
                        )
                        (nth
                            i
                            (frame1->a
                                (rep1
                                    (- i 1)
                                    (envir1-init
                                        1
                                    )
                                    (frame1-init
                                        1
                                        0
                                        0
                                        a
                                    )
                                )
                            )                           
                        )
                    )
                    (frame2-init
                        i
                        (frame1->a
                            (rep1
                                (- i 1)
                                (envir1-init
                                    1
                                )
                                (frame1-init
                                    1
                                    0
                                    0
                                    a
                                )
                            )
                        )
                    )
                )
            )
        )
    )
    :use
    (
        (:instance
            warnings-lemma-10
            (
                iteration
                (+
                    i
                    1
                )
            )
        )
    )
    :disable
    (
        rep1-lemma-8
        rep2-lemma-12
        vc-1-lemma-4
        vc-1-lemma-16
        vc-1-lemma-62
        vc-1-lemma-63
        vc-1-lemma-68
    )
)

